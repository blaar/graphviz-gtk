//
//  Link.hpp
//  e_bash
//
//  Created by Arnaud Blanchard on 30/08/2017.
//
//

#ifndef LINK_H
#define LINK_H

#include <gvc.h>
#include "Process.h"
#include "Script.h"

class Script;
class Process; 

class Link{
    int sync;
    Script *script;

public:
    Agedge_t *edge;
    char *name;
    int selected;
    Process *input_process, *output_process;

    
    Link(Script *script, Process *input_process, char *output_channel, Process *output_process, char *input_channel, int sync);
    ~Link();
    
    void edit();
    void select();
    void unselect();
    
    static Link *dialog(Script *script, Process *input_process, Process *output_process);
};

#endif /* Link_hpp */
