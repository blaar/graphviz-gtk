//
//  Menu.h
//  e_bash
//
//  Created by Arnaud Blanchard on 25/08/2017.
//
//

#ifndef MENU_H
#define MENU_H

#include "common.h"
#include <gtk/gtk.h>

typedef struct Menu{
    
    Menu(PROGRAM_TYPE menu_type);
    GtkWidget *widget;
}Menu;

#endif
