//
//  Script.hpp
//  e_bash
//
//  Created by Arnaud Blanchard on 25/08/2017.
//
//

#ifndef SCRIPT_H
#define SCRIPT_H

//#include "common.h"
#include <gtk/gtk.h>
#include <gvc.h>
#include "Program.h"
#include "Process.h"
#include "Link.h"


class Process;

class Script{
    int width;

    
public:
    int height;
    GtkWidget *drawing_area, *source_view;
    Process *initial_process;
    Link *initial_link;
    Process **processes, **selected_processes;
    int processes_nb, selected_processes_nb;
    Link **links, **selected_links;
    int links_nb, selected_links_nb;

    Agraph_t *main_graph, *input_subgraph;
    Agnode_t *last_input_node;
    GVC_t *gvc;
    GtkWidget *widget;
    static Script **array;
    static int nb;
    
    void init();

    Script();
    Script(char const *filename);

    void add_process(Program *program, char **argv);
    void add_link( Process *input_process, char *output_channel, Process *output_process, char *input_channel, int sync);
    Process *get_process(double x, double y);
    Link *get_link(double x, double y);

    Process *get_process(Agnode_t *node);
    void save_bash(char const *filename);
    void update_display();
    int unselect_all();
    
    void delete_selection();
    void generate_layout();
    void run();
    void pause();
    void stop();
    

};

//extern Script *current_script;


#endif 
