//
//  parse_help.cpp
//  e_bash
//
//  Created by Arnaud Blanchard on 23/08/2017.
//
//

#include "parse_help.h"
#include "blc_core.h"
#include <unistd.h>
#include <errno.h> //EINTR
#include <libgen.h> //basename
#include <stdio.h> //popen

//This are in blc_program
#define POSITIONAL_ARGUMENTS_TITLE "\npositional arguments:\n"
#define OPTIONAL_ARGUMENTS_TITLE "\noptional arguments:\n"

void blc_remove_spaces(char const **text)
{
    while ((*text)[0]==' ') (*text)++;
}

static void free_optional_argument(struct blc_optional_argument *argument)
{
    if (argument->shortname) FREE(argument->shortname);
    if (argument->longname) FREE(argument->longname);
    if (argument->help) FREE(argument->help);
}

static void blc_get_help(blc_mem *help, char const *directory, char const *program_name)
{
    char buffer[LINE_MAX];
    int error;
    ssize_t n;
    FILE *answer;
    
    SPRINTF(buffer,"cd %s;  ./%s -h 2>&1", directory, program_name); // The 'cd <directory>;' is necessary for the scripts to find blaar.sh ( popen use 'sh' and dos not use envirnment variables'
    SYSTEM_ERROR_CHECK(answer = popen(buffer, "r"), NULL, "Executing '%s'", buffer);
    
    do{
        SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(n=fread( buffer, 1, sizeof(buffer), answer), -1, EINTR, "Reading pipe of '%s'.", buffer);
        help->append(buffer, n);
    }while(n!=0);
    error=pclose(answer);
    help->append_text("");//Eof line
    if (error!=0) {
        PRINT_WARNING("Error '%d' executing '%s' message '%s'", error, buffer, help->chars);
        help->allocate(0);
    }
}

char *blc_parse_description(char const *str, char const **optional_argument_description, char const **positional_argument_description)
{
    char const *pt;
    char *description;
    char const *end_description;
    
    pt=strstr(str, POSITIONAL_ARGUMENTS_TITLE);
    if (pt){
        end_description=pt;
        if (positional_argument_description) *positional_argument_description=pt+strlen(POSITIONAL_ARGUMENTS_TITLE);
        pt=strstr(pt, OPTIONAL_ARGUMENTS_TITLE);
    }
    else {
        if (positional_argument_description) *positional_argument_description=NULL;
        pt=strstr(str, OPTIONAL_ARGUMENTS_TITLE);
        end_description=pt;
    }
    if (pt){
        if (optional_argument_description) *optional_argument_description=pt+strlen(OPTIONAL_ARGUMENTS_TITLE);
    }
    else
    {
        if (optional_argument_description) *optional_argument_description=NULL;
    }
    
    
    if (end_description==NULL) description=strdup(str);
    else description=strndup(str, end_description-str);
    return description;
}

/**Parse text in optional_arguments_description to find optional_arguments.
It returns the number of arguments and update pos at the end of the optional arguments bloc.*/

int blc_parse_optional_arguments(struct blc_optional_argument **results, char const **pos, char const *optional_arguments_description)
{
    char const*help;
    char token[NAME_MAX+1];
    char text[NAME_MAX+1];
    int token_size;
    int arg=1;
    int results_nb=0;
    struct blc_optional_argument argument;
    
    *results=NULL;
    
    while(arg)
    {
        help=NULL;
        blc_remove_spaces(pos);
        if (sscanf(*pos, "[--%[^] ]%n", token, &token_size)==1){
            *pos+=token_size;
            SPRINTF(text, "--%s", token);
            argument.shortname=NULL;
            argument.longname=strdup(token);
            help=strstr(optional_arguments_description, text);
        }
        else if (sscanf(*pos, "[-%[^] ]%n", token, &token_size)==1){
            *pos+=token_size;
            SPRINTF(text, "-%s", token);
            argument.shortname=strdup(token);
            argument.longname=NULL;
            if (optional_arguments_description) //Test not useful ?
            {
                help=strstr(optional_arguments_description, text);
                if (help) {
                    help+=strlen(text);
                    if (sscanf(help, ", --%s%n", token, &token_size)==1) {
                        help+=token_size;
                        argument.longname=strdup(token);
                    }
                }
            }
            
        }else break;
        token_size=0;
        sscanf(*pos, "%*[= ]%n", &token_size); //remove spaces or equal
        *pos+=token_size;
        if (sscanf(*pos, "%[^] ]%n", token, &token_size)==1) {
            *pos+=token_size;
            argument.value=strdup(token);  //FIXME: Memory may not be free
        }
        else  argument.value=NULL;
        
        if (help){
            help+=strlen(text);
            blc_remove_spaces(&help);
            if (argument.value) {
                help+=strlen(argument.value);
                blc_remove_spaces(&help);
            }
            argument.help=strndup(help, strchr(help, '\n') - help);
        }
        else argument.help=NULL;
        
        if (argument.longname==NULL || strcmp(argument.longname, "help")!=0) APPEND_ITEM(results, &results_nb, &argument);
        else free_optional_argument(&argument);
        
        token_size=0;
        sscanf(*pos, "%*[] ]%n", &token_size);
        *pos+=token_size;
    }
    return results_nb;
}

int blc_parse_positional_arguments(struct blc_positional_argument **results, char const **pos, char const *positional_arguments_description)
{
    char const *help;
    char token[NAME_MAX+1];
    int token_size;
    int results_nb=0;
    struct blc_positional_argument argument;
    
    *results=NULL;
    while (sscanf(*pos, "%[^\n ]%n", token, &token_size)==1)
    {
        *pos+=token_size;
        argument.name=strdup(token);
        argument.value=NULL;
        help=strstr(positional_arguments_description, token);
        if (help)
        {
            help+=strlen(token);
            blc_remove_spaces(&help);
            argument.help=strndup(help, strchr(help, '\n') - help);
        }
        else argument.help=NULL;
        APPEND_ITEM(results, &results_nb, &argument);
    }
    
    while (sscanf(*pos, "[%s]%n", token, &token_size)==1)
    {
        *pos+=token_size;
        argument.name=strdup(token);
        argument.value=NULL;
        help=strstr(positional_arguments_description, token) + strlen(token);
        while(help[0]==' ') help++;
        argument.help=strndup(help, strchr(help, '\n') - help);
        APPEND_ITEM(results, &results_nb, &argument);
    }
    return results_nb;
}

char const *blc_parse_help(char const *directory, char const *program_name, blc_optional_argument **optional_arguments, int *optional_arguments_nb, blc_positional_argument **positional_arguments, int *positional_arguments_nb, char **epilog)
{
    char const *str;
    char token[LINE_MAX];
    blc_mem help;
    char const *description;
    char const *description_start;
    const char *positional_argument_description;
    const char *optional_argument_description;
    (void)epilog;
    
    blc_get_help(&help, directory, program_name);
    
    if (help.chars==NULL) description=NULL;
    else {
        SPRINTF(token, "usage: %s", program_name);
        str=strstr(help.chars, token)+strlen(token);
        description_start=strchr(str, '\n')+1;
        description=blc_parse_description(description_start, &optional_argument_description, &positional_argument_description);
        *optional_arguments_nb=blc_parse_optional_arguments(optional_arguments, &str, optional_argument_description);
        *positional_arguments_nb=blc_parse_positional_arguments(positional_arguments, &str, positional_argument_description);
    }
    return description;
}
