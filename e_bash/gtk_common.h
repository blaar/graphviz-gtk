//
//  gtk_common.h
//  e_bash
//
//  Created by Arnaud Blanchard on 24/08/2017.
//
//

#ifndef GTK_COMMON_H
#define GTK_COMMON_H

#include <gtk/gtk.h>

#define STATUS_PUSH(...) status_push( __VA_ARGS__)


extern GtkWidget *general_statusbar, *tool_run_pause, *tool_stop;
extern guint statusbar_context_id;
extern GtkWindow *main_window;

void status_push(char const *format, ...);


#endif /* gtk_common_h */
