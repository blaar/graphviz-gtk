#ifndef BLC_PARSE_HELP
#define BLC_PARSE_HELP


typedef struct blc_optional_argument
{
    char const *shortname;
    char const *longname;
    char const *help;
    char const *value;
}blc_optional_argument;

typedef struct blc_positional_argument
{
    char const *name;
    char const *help;
    char const *value;
}blc_positional_argument;


char const *blc_parse_help(char const *directory, char const *program_name, blc_optional_argument **optional_arguments, int *optional_arguments_nb, blc_positional_argument **positional_arguments, int *positional_arguments_nb, char **epilog);

#endif
