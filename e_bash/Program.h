//
//  Process.hpp
//  e_bash
//
//  Created by Arnaud Blanchard on 29/08/2017.
//
//

#ifndef PROGRAM_H
#define PROGRAM_H

#include "parse_help.h"
#include <stdlib.h>

typedef enum {INPUT_PROGRAM, FUNCTION_PROGRAM } PROGRAM_TYPE;
typedef enum {INPUT_CHANNEL, OUTPUT_CHANNEL} CHANNEL_DIRECTION;

class Program
{
public:

    char const *description;
    blc_optional_argument *optional_arguments;
    int optional_arguments_nb;
    blc_positional_argument *positional_arguments;
    int positional_arguments_nb;
    
    PROGRAM_TYPE type;
    char const *name;
    char  ** argv_dialog(char **argv=NULL);
    char **input_channels, **output_channels, **input_channels_help, **output_channels_help;
    int input_channels_nb, output_channels_nb;
    
    Program(char const*program_name, PROGRAM_TYPE type, char const*description);
    Program(char const*program_name, PROGRAM_TYPE type, char const*description, blc_optional_argument *, int, blc_positional_argument *, int);
};

#endif /* PROGRAM_H */
