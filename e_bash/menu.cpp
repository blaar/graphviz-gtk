//
//  menu.cpp
//  e_bash
//
//  Created by Arnaud Blanchard on 23/08/2017.
//
//

#include "Script.h" //current_script
#include "Menu.h"
#include "common.h"
#include "blc_program.h"
#include "parse_help.h"
#include <stdio.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <libgen.h> //basename

void action_input_cb(GtkWidget *widget, Program *menu_action)
{
    char  *argv[2];
    char path[PATH_MAX+1];
    (void)widget;
    
  //  SPRINTF(path, "%s/%s", blaar_bin_dir, menu_action->program_name);
    
    argv[0]=basename(path);
    argv[1]=NULL;
    // execute_process_with_iter(create_command_line(dirname(path), argv));
}

static void add_process_cb(GtkWidget *widget,  Program *program){
    (void)widget;
    
    current_script->add_process(program, NULL);
    current_script->update_display();
}

static void process_dialog_cb(GtkWidget *widget, Program *program){
    (void)widget;
    
    current_script->add_process(program, program->argv_dialog());
    current_script->update_display();
}

static int possible_default(int positional_arguments_nb, PROGRAM_TYPE  type){
    return ((positional_arguments_nb==0 && type==INPUT_PROGRAM) || (positional_arguments_nb==1 && type==FUNCTION_PROGRAM));
}

/**Read the list of programs in directory_str and add it to the menu if the type (INPUT_PROGRAM, FUNCTION_PROGRAM) match.
 */
static void add_directory_programs(GtkWidget *menu_shell, char const *directory_str, PROGRAM_TYPE type){
    
    GtkWidget *default_item, *action_button, *menu, *menu_item;
    DIR *directory;
    struct stat status;
    struct dirent program_dirent, *result;
    
    char const *description;
    char token[NAME_MAX+1];
    int positional_arguments_nb, optional_arguments_nb;
    Program *program;
    struct blc_optional_argument *optional_arguments;
    struct blc_positional_argument *positional_arguments;
    
    SYSTEM_ERROR_CHECK(directory = opendir(directory_str), NULL, "Opening dir '%s'.", directory_str);
    do{
        if (readdir_r(directory, &program_dirent, &result) != 0)  EXIT_ON_ERROR("readdir_r error parsing dir.");
        else if (result){
            if (result->d_type==DT_REG){
                
                if (((type==INPUT_PROGRAM) && ( strncmp(result->d_name, "i_", 2)==0)) || ((type==FUNCTION_PROGRAM) && (( strncmp(result->d_name, "f_", 2)==0) ||  ( strncmp(result->d_name, "o_", 2)==0)))){
                    fstat(result->d_reclen, &status);
                    description=blc_parse_help(directory_str, result->d_name, &optional_arguments, &optional_arguments_nb, &positional_arguments, &positional_arguments_nb, NULL);
                    
                    
                    if (positional_arguments_nb || optional_arguments_nb>1){
                        program=new Program(result->d_name, type, description, optional_arguments, optional_arguments_nb, positional_arguments, positional_arguments_nb);
                      
                        if (possible_default(positional_arguments_nb, type))
                        {
                            menu=gtk_menu_new();
                            menu_item = gtk_menu_item_new_with_label(result->d_name);

                            gtk_container_add(GTK_CONTAINER(menu_shell), menu_item);
                            gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);
                            default_item = gtk_menu_item_new_with_label("default");
                            action_button = gtk_menu_item_new_with_label("options ...");
                            gtk_container_add(GTK_CONTAINER(menu), default_item);
                            gtk_container_add(GTK_CONTAINER(menu), action_button);

                            g_signal_connect(G_OBJECT(default_item), "activate", G_CALLBACK(add_process_cb), program);
                        }
                        else{
                            SPRINTF(token, "%s ...",result->d_name);
                            action_button = gtk_menu_item_new_with_label(token);
                            gtk_container_add(GTK_CONTAINER(menu_shell), action_button);
                            
                        }
                        g_signal_connect(G_OBJECT(action_button), "activate", G_CALLBACK(process_dialog_cb), program);
                    }
                    else{
                        if (possible_default(positional_arguments_nb, type)){
                            default_item = gtk_menu_item_new_with_label(result->d_name);
                            program=new Program(result->d_name, type, description);
                            gtk_container_add(GTK_CONTAINER(menu_shell), default_item);
                            g_signal_connect(G_OBJECT(default_item), "activate", G_CALLBACK(add_process_cb), program);
                        }
                    }
                }
            }
        }
    }while(result);
    closedir(directory);
}

Menu::Menu(PROGRAM_TYPE menu_type){
    
    widget=gtk_menu_new();
    
    if (menu_type==INPUT_PROGRAM){
        add_directory_programs(widget, blaar_bin_dir, INPUT_PROGRAM);
        add_directory_programs(widget, blaar_scripts_dir, INPUT_PROGRAM);
    }
    else if (menu_type==FUNCTION_PROGRAM){
        add_directory_programs(widget, blaar_bin_dir, FUNCTION_PROGRAM);
        add_directory_programs(widget, blaar_scripts_dir, FUNCTION_PROGRAM);
    }
    else EXIT_ON_ERROR("Wrong option '%d'", menu_type);
    gtk_widget_show_all(widget);
}

