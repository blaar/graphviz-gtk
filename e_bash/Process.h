//
//  Process.hpp
//  e_bash
//
//  Created by Arnaud Blanchard on 30/08/2017.
//
//

#ifndef PROCESS_H
#define PROCESS_H

#include <gvc.h>
#include "Program.h"
#include "Script.h"
#include <pthread.h>
#include <vte/vte.h>

class Script;
class Link;

class Process{
    char *path;
    int input_pipe_fileno; //Fileno of the stdout from the precedent process (pipe).
    Script *script;
    pthread_t thread;
    pid_t pid;
    VteTerminal *stdout_terminal, *stderr_terminal;
    gulong commit_handler;

public:
    Agnode_t *node;
    char  **argv;
    char *name;
    GtkWidget *err_window, *out_window;
    GtkWidget *err_toggle_button, *out_toggle_button;
    GtkWidget *pause_button, *stop_button, *hbox;


    int selected;
    Program *program;
    Link **input_links, **output_links;
    int input_links_nb, output_links_nb;
    int stdin_fileno, stdout_fileno, stderr_fileno;

    Process(Script *script, Program *program, char **argv);
    ~Process();
    void update_html();
    void update_layout();
    void edit();
    void select();
    void unselect();
    
    void run(int input_pipe);
    void pause();
    void stop();
    void watch();
    
};
#endif /* Process_hpp */
