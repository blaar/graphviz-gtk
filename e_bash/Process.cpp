
//
//  Process.cpp
//  e_bash
//
//  Created by Arnaud Blanchard on 30/08/2017.
//
//
#include "gtk_common.h"
#include "Process.h"
#include "Script.h"
#include "blc_core.h"
#include <errno.h>

#include <unistd.h> //execve
#include <vte/vte.h>
#include <sys/pipe.h>


extern char const* *environ;

static void add_ports(blc_mem *ports, char **channels , int channels_nb){
    char** channel;
    char tmp_port[NAME_MAX];
    
    if (channels_nb>1) ports->append_text("<TD><TABLE BORDER='0' CELLBORDER='1'>");
    FOR_EACH(channel, channels, channels_nb){
        if (channels_nb>1) ports->append_text("<TR>");
        SPRINTF(tmp_port, "<TD PORT='%s'> %s </TD>", *channel, *channel);
        ports->append_text(tmp_port);
        if (channels_nb>1) ports->append_text("</TR>");
    }
    if (channels_nb>1) ports->append_text("</TABLE></TD>");
    ports->append("", 1); //The stop null char
}

static size_t  try_copy_fileno_to_vte_terminal(int fileno, fd_set *fd_read_set, VteTerminal *stdout_terminal){
    char buffer[PIPE_SIZE];
    char *line, *end_pos;
    
    size_t size=0;
    
    if (fileno!=-1){
        if (FD_ISSET(fileno, fd_read_set)){
            SYSTEM_ERROR_CHECK(size=read(fileno, buffer, sizeof(buffer)), -1, "stdout");
            line=buffer;
            do {
                end_pos=(char*)memchr((void*)line, '\n', size-(line-buffer));
                if (end_pos!=NULL){
                    vte_terminal_feed(stdout_terminal, line, end_pos-line+1);
                    vte_terminal_feed(stdout_terminal, "\r", -1);
                    line=end_pos+1;
                    if ((line-buffer)==size) break;
                }
                else  vte_terminal_feed(stdout_terminal, line, size);
            }while(end_pos!=NULL);
        }
    }
    return size;
}

static void input_cp(VteTerminal *vte_terminal, gchar *text, guint size, Process *process){
    int i, last_return=0;
    
    //  FOR(i, size) if (text[i]==27) text[i]='\n';
    
    
    
    //Remove escape (27) and replace \r by \n
    for(i=0;i<size;i++) {
        if (text[i]==27) {
            memmove(&text[i], &text[i+1], size-i-1);
            size--;
        }
        else {
            if (text[i]=='\r') text[i]='\n';
            vte_terminal_feed(vte_terminal, &text[last_return], i-last_return+1);
            vte_terminal_feed(vte_terminal, "\r", -1);
        }
    }
    if (size && size-1-last_return!=0){
        vte_terminal_feed(vte_terminal, &text[last_return], size-1);
        vte_terminal_feed(vte_terminal, "\r", -1);
    }
    if (size)    SYSTEM_ERROR_CHECK(write(process->stdin_fileno, text, size), -1, NULL);
}


static int close_terminal_cb(GtkWidget *widget, GdkEvent  *event, Process *process){
    if (widget==process->out_window) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(process->out_toggle_button), 0);
    else if (widget==process->err_window) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(process->err_toggle_button), 0);
    else EXIT_ON_ERROR("widget is neither err_window or out_window");
    return TRUE;
}

void Process::update_html(){
    blc_mem input_ports, output_ports;
    char *html, *arg_text;
    char text[LINE_MAX];
    
    add_ports(&input_ports, program->input_channels, program->input_channels_nb);
    add_ports(&output_ports, program->output_channels, program->output_channels_nb);
    output_ports.append("", 1); //The stop null char
    
    arg_text=blc_create_command_line_from_argv(&argv[1]); //[1]:We do not want the program name argv[0];
    
    SPRINTF(text, "<TABLE  BORDER='0' ><TR><TD>%s</TD></TR><TR><TD>\n\
            <TABLE BORDER='2' CELLBORDER='1' CELLSPACING='0' >\
            <TR>%s<TD><FONT POINT-SIZE='24'>%s</FONT></TD>%s</TR>\
            </TABLE>\n\
            </TD></TR><TR><TD>%s</TD></TR>\
            </TABLE>",arg_text, input_ports.chars,  program->name, output_ports.chars, name);
    
    html = agstrdup_html(script->main_graph, text);
    FREE(arg_text);
    
    agset(node, (char*)"label", html);
}

static void widget_toggled_cb(GtkToggleButton *button,  GtkWidget *widget){
    
    if (gtk_toggle_button_get_active(button)) gtk_widget_show_all(widget);
    else gtk_widget_hide(widget);
}

Process::Process(Script *script, Program *program, char **argv):script(script),name(NULL),selected(0),program(program), input_links(NULL),  output_links(NULL), input_links_nb(0), output_links_nb(0), pid(-1){
    Agnode_t *previous_node=NULL;
    Agedge_t *edge;
    Process *tmp_process;
    char title[NAME_MAX];
    
    if (argv==NULL) {
        this->argv=MANY_ALLOCATIONS(2, char*);
        this->argv[0]=strdup(program->name);
        this->argv[1]=NULL;
    }
    else this->argv=argv;
    
    SYSTEM_ERROR_CHECK(asprintf(&name, "process%d", script->processes_nb), -1, NULL);
    
    if (program->type==INPUT_PROGRAM){
        previous_node=aglstnode(script->input_subgraph);
        node = agnode(script->input_subgraph, name, 1);
        if (previous_node){ //The goal of this is to force the order
            edge=agedge(script->input_subgraph, previous_node, node, NULL, 1);
            agsafeset(edge, (char*)"style", (char*)"invis", (char*)"");
        }
    }
    else node=agnode(script->main_graph, name, 1);
    
    update_html();
    
    tmp_process=this;
    APPEND_ITEM(&script->processes, &script->processes_nb, &tmp_process);
    
    hbox=gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    
    pause_button=gtk_button_new_from_icon_name("media-playback-pause", GTK_ICON_SIZE_SMALL_TOOLBAR);
    stop_button=gtk_button_new_from_icon_name("media-playback-stop", GTK_ICON_SIZE_SMALL_TOOLBAR);
    err_toggle_button=gtk_toggle_button_new();
    gtk_button_set_image(GTK_BUTTON(err_toggle_button), gtk_image_new_from_icon_name("utilities-terminal", GTK_ICON_SIZE_SMALL_TOOLBAR));
    
    err_window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    out_window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    
    stdout_terminal=VTE_TERMINAL(vte_terminal_new());
    stderr_terminal=VTE_TERMINAL(vte_terminal_new());
    
    SPRINTF(title, "%s:stdout", name);
    gtk_window_set_title(GTK_WINDOW(out_window), title);
    SPRINTF(title, "%s:stdin/stderr", name);
    gtk_window_set_title(GTK_WINDOW(err_window), title);
    
    gtk_container_add(GTK_CONTAINER(hbox), pause_button);
    gtk_container_add(GTK_CONTAINER(hbox), stop_button);
    gtk_container_add(GTK_CONTAINER(hbox), err_toggle_button);
    
    gtk_container_add(GTK_CONTAINER(out_window), GTK_WIDGET(stdout_terminal));
    gtk_container_add(GTK_CONTAINER(err_window), GTK_WIDGET(stderr_terminal));
    
    g_signal_connect(G_OBJECT(out_window), "delete-event", G_CALLBACK(close_terminal_cb), this);
    g_signal_connect(G_OBJECT(err_window), "delete-event", G_CALLBACK(close_terminal_cb), this);
    g_signal_connect(G_OBJECT(err_toggle_button), "toggled", G_CALLBACK(widget_toggled_cb), err_window);
    
    gtk_container_add(GTK_CONTAINER(script->drawing_area), hbox);
}

Process::~Process(){
    char **arg_pt;
    Process *tmp_process=this;
    
    gtk_widget_destroy(hbox);
    
    if (selected)   REMOVE_ITEM(&script->selected_processes,&script->selected_processes_nb, &tmp_process);
    REMOVE_ITEM(&script->processes, &script->processes_nb, &tmp_process);
    agdelete(script->main_graph, node);
    free(name);
    for(arg_pt=argv; *arg_pt!=NULL; arg_pt++) free(*arg_pt);
    free(argv);
}

void Process::update_layout(){
    boxf box;
    box=ND_bb(node);

    gtk_layout_move(GTK_LAYOUT(script->drawing_area), hbox, box.LL.x, script->height-box.LL.y);
    gtk_widget_show_all(hbox);
    
}

void Process::edit(){
    char **tmp_argv;
    tmp_argv=program->argv_dialog(argv);
    if (tmp_argv)  {
        gvFreeLayout(script->gvc, script->main_graph);
        argv=tmp_argv;
        update_html();
        script->generate_layout();
    }
}

void Process::select(){
    
    Process *tmp_process;
    if (selected==0){
        agset((void*) node, (char*) "fillcolor", (char*) "lightgray");
        
        tmp_process=this;
        APPEND_ITEM(&script->selected_processes, &script->selected_processes_nb, &tmp_process);
        selected=1;
    }
}

void Process::unselect(){
    Process *tmp_process;
    
    if (selected){
        agset((void*) node, (char*) "fillcolor", (char*) "white");
        
        tmp_process=this;
        REMOVE_ITEM(&script->selected_processes, &script->selected_processes_nb, &tmp_process);
        selected=0;
    }
}

static void* manage_process(void *user_data){
    Process *process=(Process*)user_data;
    process->watch();
    return NULL;
}


void Process::watch(){
    char pipe_buffer[PIPE_SIZE];
    ssize_t pipe_read_nb=0, pipe_buffer_writen_nb=0;
    int fd_max, status;
    ssize_t size=-1, written_nb, read_nb;
    fd_set fd_read_set, fd_write_set;
    
    fd_max=MAX(stdout_fileno, stderr_fileno);
    fd_max=MAX(fd_max, input_pipe_fileno);
    fd_max=MAX(fd_max, stdin_fileno);
    
    do
    {
        size=0;
        FD_ZERO(&fd_read_set);
        FD_ZERO(&fd_write_set);
        
        if (input_pipe_fileno!=-1) FD_SET(input_pipe_fileno, &fd_read_set);
        if (stdout_fileno!=-1) FD_SET(stdout_fileno, &fd_read_set);
        FD_SET(stderr_fileno, &fd_read_set);
        if (stdin_fileno!=-1 && pipe_read_nb) FD_SET(stdin_fileno, &fd_write_set);
        
        SYSTEM_ERROR_CHECK(::select(fd_max+1, &fd_read_set, &fd_write_set, NULL, NULL), -1, "select");
        
        if (stdin_fileno!=-1 && pipe_read_nb && FD_ISSET(stdin_fileno, &fd_write_set)){ //somthing to write and input ready;
            SYSTEM_ERROR_CHECK(written_nb=write(stdin_fileno, &pipe_buffer[pipe_buffer_writen_nb], pipe_read_nb-pipe_buffer_writen_nb),-1, "pipe_buffer_writen_nb '%ld', pipe_read_nb '%ld'", pipe_buffer_writen_nb, pipe_read_nb);
            pipe_buffer_writen_nb+=written_nb;
            size+=written_nb;
        }
        
        if (input_pipe_fileno!=-1 && FD_ISSET(input_pipe_fileno, &fd_read_set)){
            SYSTEM_ERROR_CHECK(read_nb=read(input_pipe_fileno, &pipe_buffer[pipe_read_nb], sizeof(pipe_buffer)-pipe_read_nb), -1, "input pipe");
            pipe_read_nb+=read_nb;
            size+=read_nb;
        }
        
        if (pipe_read_nb && pipe_buffer_writen_nb==pipe_read_nb){ //Everything has been written
            pipe_buffer_writen_nb=0;
            pipe_read_nb=0;
        }
        size+=try_copy_fileno_to_vte_terminal(stdout_fileno, &fd_read_set, stdout_terminal);
        size+=try_copy_fileno_to_vte_terminal(stderr_fileno, &fd_read_set, stderr_terminal);
    }
    while(size!=0);
    
    close(stdout_fileno);
    close(stderr_fileno);
    
    waitpid(pid, &status, 0);
    pid=0;
    
    g_signal_handler_disconnect(G_OBJECT(stderr_terminal), commit_handler);
    
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(err_toggle_button))) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(err_toggle_button), false);
    
    
    if (status != 0) STATUS_PUSH("Error '%d'", status);
    else  STATUS_PUSH("Stopping '%s'", name);
}

void Process::run(int input_pipe_fileno){//Associate input_fileno with stdin
    int in_pipe[2], out_pipe[2], err_pipe[2];
    
    if (pid!=-1){
        SYSTEM_ERROR_CHECK(kill(pid, SIGCONT), -1, "Continue (SIGCONT) '%s'", name);
    }
    else{
        this->input_pipe_fileno=input_pipe_fileno;
        
        SYSTEM_ERROR_CHECK(pipe(in_pipe ), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(out_pipe), -1, NULL);
        SYSTEM_ERROR_CHECK(pipe(err_pipe), -1, NULL);
        
        pid=fork();
        
        if (pid==0){
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(in_pipe[0] , STDIN_FILENO ), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(out_pipe[1], STDOUT_FILENO), -1, EINTR, NULL);
            SYSTEM_ERROR_RETRY_ON_SPECIFIC_ERRNO(dup2(err_pipe[1], STDERR_FILENO), -1, EINTR, NULL);
            
            blc_close_pipe(in_pipe);
            blc_close_pipe(out_pipe);
            blc_close_pipe(err_pipe);
            
            SYSTEM_ERROR_CHECK(execvp(program->name, argv), -1, "Executing process '%s', program '%s'", name, program->name);
        }
        close(in_pipe[0]);
        close(out_pipe[1]);
        close(err_pipe[1]);
        
        stdin_fileno=in_pipe[1];
        stdout_fileno=out_pipe[0];
        stderr_fileno=err_pipe[0];
        
        commit_handler=g_signal_connect(G_OBJECT(stderr_terminal), "commit", G_CALLBACK(input_cp), this);
        
        pthread_create(&thread, NULL, manage_process, (void*)this);
    }
}

void Process::pause(){//Associate input_fileno with stdin
    SYSTEM_ERROR_CHECK(kill(pid, SIGTSTP),-1, "Pausing (SIGTSTP) '%s'", name);
}

void Process::stop(){//Associate input_fileno with stdin
    SYSTEM_ERROR_CHECK(kill(pid, SIGTERM),-1, "Terminating (SIGTERM) '%s'", name);
}



