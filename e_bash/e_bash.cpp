
#include "common.h"
#include "gtk_common.h"
#include "Script.h"
#include "Menu.h"
#include "blc_program.h"
#include "libgen.h"

char const *blaar_dir;
char blaar_bin_dir[PATH_MAX];
char blaar_scripts_dir[PATH_MAX];

GtkWidget *general_statusbar, *notebook, *tool_run_pause, *tool_stop;
GtkWindow *main_window;
guint statusbar_context_id;

GtkApplication *app;
Script *current_script=NULL;

static int graphviz_error(char *error_message){
    EXIT_ON_ERROR("graphviz error:%s", error_message);
    return 0;
}

static void open_graphviz (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app);
static void save_graphviz (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app);
static void save_bash (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app);
static void delete_selection(GSimpleAction *action,                 GVariant      *parameter,              gpointer       app);

static GActionEntry app_entries[] =
{
    { "open_graphviz", open_graphviz, NULL, NULL, NULL },
    
    //  { "save_graphviz", view_dot_activated, NULL, NULL, NULL },
};

static GActionEntry win_entries[] =
{
    { "save_bash", save_bash, NULL, NULL, NULL },
    { "export_graphviz", save_graphviz, NULL, NULL, NULL },
    { "delete", delete_selection, NULL, NULL, NULL },

};

static void open_graphviz (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app)
{
    gint response_id;
    gchar *g_filename;
    GtkFileChooserNative *native;
    
    native = gtk_file_chooser_native_new ("Open graphviz", GTK_WINDOW(main_window),   GTK_FILE_CHOOSER_ACTION_OPEN, "_Open", "_Cancel");
    
    response_id=gtk_native_dialog_run(GTK_NATIVE_DIALOG (native));
    
    if (response_id == GTK_RESPONSE_ACCEPT){
        
        g_filename=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (native));
        current_script=new Script(g_filename);
        
        gtk_notebook_append_page(GTK_NOTEBOOK(notebook), current_script->widget, gtk_label_new(basename(g_filename)));
        gtk_widget_show_all(current_script->widget);
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), Script::nb-1);
        current_script->update_display();

        g_free(g_filename);
        
    }
    g_object_unref(native);
}

static void save_graphviz (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app)
{
    gchar *g_filename;
    gint response_id;
    GtkFileChooserNative *native;
    
    native = gtk_file_chooser_native_new ("Export Graphviz", NULL,   GTK_FILE_CHOOSER_ACTION_SAVE, "_Save", "_Cancel");
    
    response_id=gtk_native_dialog_run (GTK_NATIVE_DIALOG (native));
    
    if (response_id == GTK_RESPONSE_ACCEPT)
    {
        g_filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (native));
        gvFreeLayout(current_script->gvc, current_script->main_graph);
        gvLayout(current_script->gvc, current_script->main_graph, "dot");
        gvRenderFilename(current_script->gvc, current_script->main_graph, "dot", g_filename);
        g_free(g_filename);
    }
    g_object_unref (native);
}

static void delete_selection(GSimpleAction *action,                 GVariant      *parameter,              gpointer       app){
    
    current_script->delete_selection();
}

static void save_bash (GSimpleAction *action,                 GVariant      *parameter,              gpointer       app)
{
    gchar *g_filename;
    gint response_id;
    GtkFileChooserNative *native;
    
    native = gtk_file_chooser_native_new ("Export bash", NULL,   GTK_FILE_CHOOSER_ACTION_SAVE, "_Save", "_Cancel");
    response_id=gtk_native_dialog_run (GTK_NATIVE_DIALOG (native));
    
    if (response_id == GTK_RESPONSE_ACCEPT){
        g_filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (native));
        current_script->save_bash(g_filename);
        g_free(g_filename);
    }
    g_object_unref (native);
}

static void quit_activated (GSimpleAction *action,          GVariant      *parameter,              gpointer       app)
{
    g_application_quit (G_APPLICATION (app));
}

gboolean focus_tab_cb (GtkNotebook *notebook, GtkNotebookTab arg1,  gpointer user_data){
    int script_index;
    script_index=gtk_notebook_get_current_page(notebook);
    current_script=Script::array[script_index];
    return TRUE;
}

static void play_pause_cb(GtkToggleToolButton *button, Script *script){
    
    if (gtk_toggle_tool_button_get_active(button)){
        script->run();
        gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(tool_run_pause), "media-playback-pause");

    }
    else {
        script->pause();
        gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(tool_run_pause), "media-playback-start");
    }
}

static void activate_cb(GApplication *app){
    GdkDisplay *main_display;
    GMenu *menubar, *file_menu, *edit_menu;
    GtkWidget *grid;
    GtkWidget  *toolbar, *tool_function_menu, *tool_menu;
    Menu input_menu(INPUT_PROGRAM);
    Menu function_menu(FUNCTION_PROGRAM);

    int statusbar_context_id;
    main_display = gdk_display_get_default();
    
    /*GdkDeviceManager *device_manager = gdk_display_get_device_manager(main_display);
     pointer_device = gdk_device_manager_get_client_pointer(device_manager);
     */
    
    current_script=new Script();
    //    shell_menu=g_menu_new();
    
    main_window=GTK_WINDOW(gtk_application_window_new(GTK_APPLICATION(app)));
    gtk_window_set_title(main_window, "bash editor");
    gtk_widget_set_size_request(GTK_WIDGET(main_window), 800, 600);

    
    notebook=gtk_notebook_new();
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), GTK_WIDGET(current_script->widget), gtk_label_new("Unititled"));
    g_signal_connect(G_OBJECT(notebook), "focus-tab", G_CALLBACK(focus_tab_cb), NULL);
    
    menubar=g_menu_new();
    file_menu=g_menu_new();
    edit_menu=g_menu_new();
    
    g_menu_append_submenu(menubar, "File", G_MENU_MODEL(file_menu));
    g_menu_append(file_menu,  "Open graphviz",  "app.open_graphviz"); //win. is important as it means associated to the current window, use app for application action.
    g_menu_append(file_menu,  "Save bash ...",  "win.save_bash"); //win. is important as it means associated to the current window, use app for application action.
    g_menu_append(file_menu,  "Export graphviz ...",  "win.export_graphviz"); //win. is important as it means associated to the current window, use app for application action.

    g_menu_append_submenu(menubar, "Edit", G_MENU_MODEL(edit_menu));
    g_menu_append(edit_menu,  "Delete",  "win.delete"); //win. is important as it means associated to the current window, use app for application action.

    
    //    g_menu_append(file_menu,  "Save graphviz",  "app.save_graphviz");
    g_action_map_add_action_entries (G_ACTION_MAP (app), app_entries, G_N_ELEMENTS (app_entries), app);
    g_action_map_add_action_entries (G_ACTION_MAP (main_window), win_entries, G_N_ELEMENTS (win_entries), main_window);
    
    gtk_application_set_menubar(GTK_APPLICATION (app), G_MENU_MODEL(menubar));
    
    toolbar=gtk_toolbar_new();
    
    
    tool_menu=GTK_WIDGET(gtk_menu_tool_button_new(gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_SMALL_TOOLBAR), "input"));
    gtk_menu_tool_button_set_menu(GTK_MENU_TOOL_BUTTON(tool_menu), input_menu.widget);
    tool_function_menu=GTK_WIDGET(gtk_menu_tool_button_new(gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_SMALL_TOOLBAR), "function"));
    gtk_menu_tool_button_set_menu(GTK_MENU_TOOL_BUTTON(tool_function_menu), function_menu.widget);
    
    
    tool_run_pause=GTK_WIDGET(gtk_toggle_tool_button_new());
    tool_stop=GTK_WIDGET(gtk_tool_button_new(gtk_image_new_from_icon_name("media-playback-stop", GTK_ICON_SIZE_SMALL_TOOLBAR), "stop"));

    gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(tool_run_pause), "media-playback-start");
    
    gtk_widget_set_sensitive(tool_stop, FALSE);

    grid=gtk_grid_new();
    gtk_orientable_set_orientation(GTK_ORIENTABLE(grid), GTK_ORIENTATION_VERTICAL);
    
    general_statusbar=gtk_statusbar_new();
    statusbar_context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(general_statusbar), "default");
    gtk_container_add(GTK_CONTAINER(toolbar), tool_menu);

    gtk_container_add(GTK_CONTAINER(toolbar), tool_function_menu);
    gtk_container_add(GTK_CONTAINER(toolbar), tool_run_pause);
    gtk_container_add(GTK_CONTAINER(toolbar), tool_stop);
    
    gtk_container_add(GTK_CONTAINER(grid), toolbar);


    gtk_container_add(GTK_CONTAINER(grid), notebook);
    gtk_container_add(GTK_CONTAINER(grid), general_statusbar);

    gtk_container_add(GTK_CONTAINER(main_window), grid);
    gtk_widget_show_all(GTK_WIDGET(main_window));
    
    
    g_signal_connect(G_OBJECT(tool_run_pause), "toggled", G_CALLBACK(play_pause_cb), current_script);

    //    if (keyboard_mode) g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK (on_key_press), NULL);
}

int main(int argc, char **argv)
{
    int status;
    GtkApplication *app;
    char *path, *var, *blaar_dir;
    
    // blc_program_set_description("Graphical interface for blaar programs");
    // blc_program_init(&argc, &argv, blc_quit);
    blaar_dir=getenv("BLAAR_DIR");
    if (blaar_dir==NULL) EXIT_ON_ERROR("Your environment variable 'BLAAR_DIR' is not defined.");
    SPRINTF(blaar_bin_dir, "%s/bin", blaar_dir);
    SPRINTF(blaar_scripts_dir, "%s/scripts", blaar_dir);
    
    //Define the graphviz message error
  //  agseterrf(graphviz_error);
    
    
    path=getenv("PATH");
    blaar_dir=getenv("BLAAR_DIR");
    asprintf(&var, "%s:%s/bin:%s/scripts", path, blaar_dir, blaar_dir);
    setenv("PATH", var, 1);
    FREE(var);
    
    gtk_disable_setlocale();
    gtk_init(&argc, &argv);
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    
    g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
    status = g_application_run(G_APPLICATION(app), 0, NULL);
    g_object_unref(app);
    
    return 0;
}


