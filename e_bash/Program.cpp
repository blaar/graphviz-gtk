//
//  Process.cpp
//  e_bash
//
//  Created by Arnaud Blanchard on 29/08/2017.
//
//

#include "Program.h"
#include "common.h"

#include "gtk_common.h"
#include <blc_core.h>
#include <string.h>


static void toggled_option_cb(GtkToggleButton *button, gpointer widget){
    gtk_widget_set_sensitive(GTK_WIDGET(widget), gtk_toggle_button_get_active(button));
}

Program::Program(char const *program_name, PROGRAM_TYPE type, char const *description):  description(description), type(type){
    this->name=strdup(program_name);
}

Program::Program(char const*program_name, PROGRAM_TYPE type, char const*description, blc_optional_argument *optional_arguments, int optional_arguments_nb, blc_positional_argument *positional_arguments, int positional_arguments_nb)
: description(description), optional_arguments(optional_arguments), optional_arguments_nb(optional_arguments_nb), positional_arguments(positional_arguments), positional_arguments_nb(positional_arguments_nb), type(type)
{
    blc_positional_argument *positional_argument;
    blc_optional_argument *optional_argument;
    char const *port_name;
    
    input_channels_nb=0;
    input_channels=NULL;
    output_channels_nb=0;
    output_channels=NULL;
    
    this->name=strdup(program_name);
    
    FOR_EACH(positional_argument, positional_arguments, positional_arguments_nb){
        if (strcmp(positional_argument->name, "blc_channel-in")==0){
            asprintf((char**)&port_name, "%d", input_channels_nb);
            APPEND_ITEM(&input_channels, &input_channels_nb, &port_name);
        }
        else if (strcmp(positional_argument->name, "blc_channel-out")==0){
            asprintf((char**)&port_name, "%d", output_channels_nb);
            APPEND_ITEM(&output_channels, &output_channels_nb, &port_name);
        }
    }
    
    FOR_EACH(optional_argument, optional_arguments, optional_arguments_nb){
        if (optional_argument->value){
            if (strcmp(optional_argument->value, "blc_channel-in")==0){
                if (optional_argument->longname) port_name=optional_argument->longname;
                else port_name=optional_argument->shortname;
                APPEND_ITEM(&input_channels, &input_channels_nb, &port_name);
            }
            else if (strcmp(optional_argument->value, "blc_channel-out")==0){
                if (optional_argument->longname) port_name=optional_argument->longname;
                else port_name=optional_argument->shortname;
                APPEND_ITEM(&output_channels, &output_channels_nb, &port_name);
            }
        }
    }
}

static void remove_args(char ***argv, char **arg_pt, int args_to_remove_nb){
    int  i, argc;
    
    for (argc=0; argv[argc]!=NULL; argc++); //We ount current arg_nb
    
    for(i=0; arg_pt[i+args_to_remove_nb]!=NULL; i++){
        arg_pt[i]=arg_pt[i+args_to_remove_nb];
    }
    arg_pt[i]=NULL;
    
    MANY_REALLOCATIONS(argv, argc-args_to_remove_nb+1);
}

static char const* find_option(blc_optional_argument *option, char ***argv){
    
    char **arg_pt;
    char const *arg=NULL;
    
    for (arg_pt=*argv; *arg_pt!=NULL; arg_pt++){
        if (strncmp(*arg_pt, "--", 2)== 0){ //It may be a long option
            if (strncmp((*arg_pt)+2, option->longname, strlen(option->longname))==0) {
                if (option->value) {
                    arg=strpbrk(*arg_pt, " =");
                    while(arg[0]==' ') arg++; //Remove spaces
                    remove_args(argv, arg_pt, 1);
                    break;
                }
                else {
                    arg="1"; //It is a flag
                    remove_args(argv, arg_pt, 1);
                    break;
                }
            }
        }
        else if ((*arg_pt)[0]=='-'){ //It may be a short option
            if ((*arg_pt)[1]==option->shortname[0]){
                if (option->value){
                    arg=*arg_pt+2; //We remove first two characters ( -<letter> ) and remove spaces after if there are
                    while(arg[0]==' ') arg++;
                    remove_args(argv, arg_pt, 1);
                    break;
                }
                else {
                    arg="1"; //It is a flag
                    remove_args(argv, arg_pt, 1);
                    break;
                }
            }
        }
    }
    return arg; //option not found
}

/**Display a dialog menu with all the possible options and parameters
 If exisiting_argv is not NULL, the default options are already filled
 */
char  **Program::argv_dialog(char **existing_argv)
{
    blc_optional_argument *option;
    char const *pos;
    char const *value;
    char *arg;
    char const *existing_value;
    char **argv=NULL;
    int argc, existing_id;
    int index;
    
    char token[NAME_MAX+1];
    char text[NAME_MAX+1];
    int line_id;
    
    GtkWidget **optional_displays=NULL, **positional_displays;
    int token_size;
    GtkWidget  *dialog, *box;
    GtkWidget  *parameter_label, *description_label, *grid, *help_label;
    int i;
    
    dialog=gtk_dialog_new_with_buttons(name, main_window, GTK_DIALOG_MODAL,  "_OK", GTK_RESPONSE_ACCEPT, "_Cancel", GTK_RESPONSE_REJECT, NULL);
    box=gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    description_label=gtk_label_new(description);
    grid=gtk_grid_new();
    
    line_id=0;
    
    optional_displays=MANY_ALLOCATIONS(optional_arguments_nb, GtkWidget*);
    FOR(i, optional_arguments_nb) {
        option=&optional_arguments[i];
        
        if (existing_argv) existing_value=find_option(option, &existing_argv);
        else existing_value=NULL;
        
        if (option->value==NULL) optional_displays[i]=gtk_label_new(""); //This is a flag
        else {
            if (strchr(option->value, '|') || existing_value){
                index=0;
                pos=option->value;
                optional_displays[i]=gtk_combo_box_text_new_with_entry();
                token_size=0;
                sscanf(pos, "%*[ =]%n", &token_size); //remove spaces or equal
                pos+=token_size;
                existing_id=-1;
                while(sscanf(pos, "%[^| ]%n", token, &token_size)==1) {
                    pos+=token_size;
                    token_size=0;
                    sscanf(pos, "%*[| ]%n", &token_size); //remove spaces or equal
                    pos+=token_size;
                    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(optional_displays[i]), NULL, token);
                    if ((existing_value) && (strcmp(token, existing_value)==0))  gtk_combo_box_set_active(GTK_COMBO_BOX(optional_displays[i]), index);
                    index++;
                }
                
                if (existing_value==NULL) gtk_combo_box_set_active(GTK_COMBO_BOX(optional_displays[i]), 0);
                else {
                    if (gtk_combo_box_get_active(GTK_COMBO_BOX(optional_displays[i]))==-1){
                        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(optional_displays[i]), NULL, existing_value);
                        gtk_combo_box_set_active(GTK_COMBO_BOX(optional_displays[i]), index);
                    }
                }
            }
            else {
                optional_displays[i]=gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(optional_displays[i]), option->value);
            }
        }
        
        if (option->longname) SPRINTF(text, "--%s", option->longname);
        else SPRINTF(text, "-%s", option->shortname);
        
        parameter_label=gtk_toggle_button_new_with_label(text);
        gtk_widget_set_hexpand(parameter_label, 1);
        gtk_widget_set_halign(parameter_label, GTK_ALIGN_START);
        gtk_grid_attach(GTK_GRID(grid), parameter_label, 0, line_id, 1, 1);
        help_label=gtk_label_new(option->help);
        gtk_widget_set_halign(help_label, GTK_ALIGN_START);
        if (optional_displays[i]) gtk_grid_attach(GTK_GRID(grid), GTK_WIDGET(optional_displays[i]), 1, line_id, 1, 1);
        //    else  optional_displays[i]=help_label;
        
        if (existing_value==NULL) gtk_widget_set_sensitive(GTK_WIDGET(optional_displays[i]), FALSE);
        else gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(parameter_label), TRUE);
        
        g_signal_connect(G_OBJECT(parameter_label), "toggled", G_CALLBACK(toggled_option_cb), optional_displays[i]);
        
        gtk_grid_attach(GTK_GRID(grid), help_label, 2, line_id, 1, 1);
        line_id++;
    }
    
    positional_displays=MANY_ALLOCATIONS(positional_arguments_nb, GtkWidget*);
    FOR(i, positional_arguments_nb) {
        parameter_label=gtk_label_new(positional_arguments[i].name);
        positional_displays[i]=gtk_entry_new();
        
        if (existing_argv && existing_argv[1]) {
            gtk_entry_set_text(GTK_ENTRY(positional_displays[i]), existing_argv[1]); //0 is the program name
            remove_args(&existing_argv, &existing_argv[1], 1);
        }
        else if (positional_arguments[i].value) gtk_entry_set_text(GTK_ENTRY(positional_displays[i]), positional_arguments[i].value);
        
        gtk_widget_set_hexpand(parameter_label, 0);
        
        gtk_widget_set_halign(parameter_label, GTK_ALIGN_START);
        gtk_grid_attach(GTK_GRID(grid), parameter_label, 0, line_id, 1, 1);
        gtk_grid_attach(GTK_GRID(grid), positional_displays[i], 1, line_id, 1, 1);
        line_id++;
    }
    
    gtk_label_set_line_wrap (GTK_LABEL(description_label), 1);
    gtk_widget_set_halign(description_label, GTK_ALIGN_START);
    
    gtk_container_add(GTK_CONTAINER(box), description_label);
    gtk_container_add(GTK_CONTAINER(box), grid);
    
    gtk_widget_show_all(dialog);
    
    switch (gtk_dialog_run(GTK_DIALOG(dialog)))
    {
        case GTK_RESPONSE_ACCEPT:
            //       SPRINTF(path, "%s/%s", functions_dir, program_name);
            argv=NULL;
            argc=0;
            value=strdup(name);
            APPEND_ITEM(&argv, &argc, &value);
            FOR(i, optional_arguments_nb){
                if( gtk_widget_get_sensitive(GTK_WIDGET(optional_displays[i]))) {
                        if (GTK_IS_ENTRY(optional_displays[i])) value=strdup(gtk_entry_get_text(GTK_ENTRY(optional_displays[i])));
                        else if (GTK_IS_COMBO_BOX_TEXT(optional_displays[i])) value=strdup(gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(optional_displays[i])));
                        else value=NULL; //It is a flag
                    
                    if (optional_arguments[i].shortname){
                        if (value==NULL) SYSTEM_ERROR_CHECK(asprintf(&arg, "-%s", optional_arguments[i].shortname), -1, NULL);
                        else{
                            if (strchr(value, ' ')) SYSTEM_ERROR_CHECK(asprintf(&arg, "-%s\"%s\"", optional_arguments[i].shortname, value), -1, NULL); //protect spaces
                            else SYSTEM_ERROR_CHECK(asprintf(&arg, "-%s%s", optional_arguments[i].shortname, value), -1, NULL);
                        }
                    }
                    else if (optional_arguments[i].longname){
                        if (value==NULL) SYSTEM_ERROR_CHECK(asprintf(&arg, "--%s", optional_arguments[i].longname), -1, NULL);
                        else {
                            if (strchr(value, ' ')) SYSTEM_ERROR_CHECK(asprintf(&arg, "--%s=\"%s\"", optional_arguments[i].longname,  value), -1, NULL); //protect spaces
                            else SYSTEM_ERROR_CHECK(asprintf(&arg, "--%s=%s", optional_arguments[i].longname,  value), -1, NULL);
                        }
                    }
                    else EXIT_ON_ERROR("An option must have at least a short or long name");
                    APPEND_ITEM(&argv, &argc, &arg);
                }
            }
            
            FOR(i, positional_arguments_nb){
                value=strdup(gtk_entry_get_text(GTK_ENTRY(positional_displays[i])));
                APPEND_ITEM(&argv, &argc, &value);
            }
            arg=NULL;
            APPEND_ITEM(&argv, &argc, &arg);
            //          execute_process_with_iter(create_command_line(dirname(path), argv));
            
            break;
        case GTK_RESPONSE_CANCEL:break;
    }
    
    gtk_widget_destroy (dialog);
    
    if (existing_argv){
        for (i=0; existing_argv[i]!=NULL; i++) free(existing_argv[i]);
    }
    FREE(existing_argv);
    FREE(optional_displays);
    FREE(positional_displays);
    return argv;
}



