//
//  common.h
//  e_bash
//
//  Created by Arnaud Blanchard on 25/08/2017.
//
//

#ifndef COMMON_H
#define COMMON_H

#include <sys/syslimits.h> //PATH_MAX

#include "Script.h"

extern char blaar_bin_dir[PATH_MAX];
extern char blaar_scripts_dir[PATH_MAX];

extern Script *current_script;


#endif
