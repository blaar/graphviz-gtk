
#include "common.h"
#include "gtk_common.h"
#include <gtksourceview/gtksource.h>
#include "Script.h"
#include "blc_core.h"
#include <stdarg.h>
#include "Process.h"
#include "Link.h"


Script **Script::array=NULL;
int Script::nb=0;

void status_push(char const *format, ...){
    va_list arguments;
    char *message=NULL;
    va_start(arguments, format);
    vasprintf(&message, format, arguments);
    gtk_statusbar_push(GTK_STATUSBAR(general_statusbar), statusbar_context_id, message);
    FREE(message);
    va_end(arguments);
}

typedef enum {
    ACTION_NOTHING = 0, ACTION_CREATE_LINK, ACTION_CREATE_LINK_AND_PROCESS, ACTION_SELECT_PROCESS
} ACTION_TYPE;

ACTION_TYPE action;

static int motion_gtk_cb(GtkWidget *widget, GdkEventButton *event, Script *script){
    Process *process;
    (void) widget;
    
    if (script->initial_process)
    {
        process = script->get_process(event->x, event->y);
        
        if (process == NULL)
        {
            STATUS_PUSH("Release button to create a link from '%s' to a new process.", script->initial_process->name);
            action = ACTION_CREATE_LINK_AND_PROCESS;
        }
        else if (process==script->initial_process)
        {
            if (action == ACTION_CREATE_LINK_AND_PROCESS){
                STATUS_PUSH("Release button to create a recursive link to '%s'.", script->initial_process->name);
                action = ACTION_CREATE_LINK;
            }
        }
        else
        {
            action = ACTION_CREATE_LINK;
            STATUS_PUSH("Release button to create a link from '%s' to '%s'.", script->initial_process->name, process->name);
        }
    }
    
    return TRUE;
}



static int button_press_gtk_cb(GtkWidget *widget, GdkEventButton *event, Script *script){
    Process *process=NULL;
    Link *link=NULL;
    
    if (event->button == 1){
        process=script->get_process(event->x, event->y);
        if (process==NULL) link=script->get_link(event->x, event->y);
        
        if (event->type == GDK_BUTTON_PRESS){
            action = ACTION_NOTHING;
            
            script->initial_link=link;
            if (process){
                STATUS_PUSH(process->name);
                script->initial_process=process;
            }
            else if (link) STATUS_PUSH(link->name);
        }
        else
            if (event->type == GDK_2BUTTON_PRESS){
                if (process && process==script->initial_process){
                    process->edit();
                }
                else if (link && link==script->initial_link){
                    link->edit();
                }
                /*           if (initial_link != NULL) initial_link->edit();
                 else if (initial_group != NULL){
                 strcpy(previous_group_name, initial_group->no_name);
                 if (initial_group->edit())
                 {
                 agdelete(leto->main_graph, initial_group->ag_node);
                 initial_group->add_to_graph(leto->graphs[initial_group->ech_temps]);
                 leto->update_links_with_new_group_name(previous_group_name, initial_group->no_name);
                 
                 leto->update_layout();
                 }
                 }
                 }
                 else if (event->type == GDK_BUTTON_PRESS)
                 {*/
            }
    }
    
    return TRUE;
}

Process *Script::get_process(Agnode_t *node){
    Process **process_pt;
    FOR_EACH(process_pt, processes, processes_nb) if (process_pt[0]->node == node) break;
    if (process_pt==processes+processes_nb) return NULL;
    else return process_pt[0];
    
}

static int button_release_gtk_cb(GtkWidget *widget, GdkEventButton *event,  Script *script){
    Link *link;
    Process *process;
    char *output_channel=NULL, *input_channel=NULL;
    Process *input_process, *output_process;
    
    int need_redraw = 0;
    (void) widget;
    
    process=script->get_process(event->x, event->y);
    if (process==NULL) link=script->get_link(event->x, event->y);
    
    if (event->button == 1){
        switch (action){
            case ACTION_NOTHING:
                if ((process) && (process == script->initial_process)){
                    if (event->state & GDK_CONTROL_MASK){
                        if (process->selected) process->unselect();
                        else process->select();
                        need_redraw=1;
                        
                    }
                    else{
                        script->unselect_all();
                        process->select();
                        need_redraw=1;
                    }
                }
                else
                    if (link && link== script->initial_link){
                        if (event->state & GDK_CONTROL_MASK){
                            if (link->selected) link->unselect();
                            else link->select();
                            need_redraw=1;
                            
                        }
                        else{
                            script->unselect_all();
                            link->select();
                            need_redraw=1;
                        }
                    } else
                    {
                        if (script->unselect_all()) need_redraw=1;
                    }
                
                break;
                
            case ACTION_CREATE_LINK:
                input_process=script->initial_process;
                output_process=process;
                
                if (input_process->program->output_channels_nb==0) STATUS_PUSH("'%s' does not have ouput channel", input_process->program->name);
                else if (input_process->program->output_channels_nb==1) output_channel=input_process->program->output_channels[0];
                
                if (output_process->program->input_channels_nb==0) STATUS_PUSH("'%s' does not have input channel", output_process->program->name);
                else if (output_process->program->input_channels_nb==1) input_channel=output_process->program->input_channels[0];
                
                
                if ((input_channel) && (output_channel)) {
                    script->add_link(input_process, output_channel,  output_process, input_channel, 1);
                }
                else Link::dialog(script, input_process, output_process);
                
                script->update_display();
                
                action=ACTION_NOTHING;
                
                /*
                 new_link = script->add_link_dialog(initial_group, final_group);
                 if (new_link != NULL)
                 {
                 blc_say("link_created between %s and %s created.", initial_group->no_name, final_group->no_name);
                 set_selected_link(new_link);
                 }
                 else blc_say("Link creation canceled");
                 set_selected_link(new_link);
                 type_group::unselect_all();*/
                break;
                /*
                 case LETO_ACTION_CREATE_LINK_AND_GROUP:
                 blc_say("Creating a new group");
                 new_group = script->add_group_dialog();
                 if (new_group != NULL)
                 {
                 blc_say("A new group '%s' has been created, creating a new link.", new_group->no_name);
                 if (initial_group != NULL) new_link = script->add_link_dialog(initial_group, new_group);
                 else if (final_group != NULL) new_link = script->add_link_dialog(new_group, final_group);
                 if (new_link != NULL)
                 {
                 blc_say("A new link has been created.");
                 }
                 else blc_say("Link creation canceled.", new_group->no_name);
                 }
                 else blc_say("Group creation canceled");
                 set_selected_link(new_link);
                 type_group::unselect_all();
                 new_group->select();
                 break;*/
        }
    }
    if (need_redraw)  gtk_widget_queue_draw(script->widget);
    script->initial_link=NULL;
    script->initial_process=NULL;
    
    return TRUE;
}


void Script::delete_selection(){
    
    Link **link_pt;
    
    gvFreeLayout(gvc, main_graph);
    while(selected_processes) {
        
        //We slect the links associated to the process in order to remove them
        FOR_EACH(link_pt, links, links_nb) if ((link_pt[0]->input_process==selected_processes[0]) || (link_pt[0]->output_process==selected_processes[0])) link_pt[0]->select();
        delete selected_processes[0];
    }
    while(selected_links) delete selected_links[0];
    generate_layout();
}



static void draw_callback (GtkWidget *widget, cairo_t *cr, Script *script){
    Process **process_pt;
    gvRenderContext(script->gvc, script->main_graph, "png:cairo", cr); // FIXME: Trick The export of cairo to png stdout has been desactivated in the graphviz plugin (gvrender_pango.c)
}

void Script::init(){
    GtkWidget *drawing_area_scrolled_window, scrolled_window2;
    GtkTextBuffer text_buffer;
    GtkSourceLanguage *language;
    GtkSourceLanguageManager *manager;
    
    char const * const*ids;
    int i;
    
    processes=NULL;
    processes_nb=0;
    links=NULL;
    links_nb=0;
    
    selected_processes=NULL;
    selected_processes_nb=0;
    selected_links=NULL;
    selected_links_nb=0;
    
    Script *script=this; //we cannot use &this
    gvc=gvContext();
    last_input_node=NULL;
    
    APPEND_ITEM(&array, &nb, &script);
    
    widget=gtk_paned_new(GTK_ORIENTATION_VERTICAL);
    drawing_area_scrolled_window=gtk_scrolled_window_new(NULL, NULL);
    source_view=gtk_source_view_new();

    drawing_area=gtk_layout_new(NULL, NULL);
    gtk_widget_set_hexpand(drawing_area, 1);
    gtk_widget_set_vexpand(drawing_area, 1);
    gtk_widget_set_events(drawing_area, GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_SCROLL_MASK );
    gtk_container_add(GTK_CONTAINER(drawing_area_scrolled_window), drawing_area);
    
    gtk_container_add(GTK_CONTAINER(widget), drawing_area_scrolled_window);
    gtk_container_add(GTK_CONTAINER(widget), source_view);
    
    g_signal_connect(drawing_area, "draw", G_CALLBACK(draw_callback), this);
    g_signal_connect(drawing_area, "button-press-event", G_CALLBACK(button_press_gtk_cb), this);
    g_signal_connect(drawing_area, "button-release-event", G_CALLBACK(button_release_gtk_cb), this);
    g_signal_connect(drawing_area, "motion-notify-event", G_CALLBACK(motion_gtk_cb), this);
}

Script::Script(){
    
    init();
    main_graph = agopen((char*)"main", Agdirected, NULL);
    
    //Graph attribute
    agattr(main_graph, AGRAPH, (char*)"rankdir", (char*)"LR");
    agattr(main_graph, AGRAPH, (char*)"bgcolor", (char*)"transparent");
    agattr(main_graph, AGRAPH, (char*)"resolution", (char*)"72");
    agattr(main_graph, AGRAPH, (char*)"splines", (char*)"true");
    agattr(main_graph, AGRAPH, (char*)"overlap",  (char*)"false");
    //    agattr(main_graph, AGRAPH, (char*)"compound", (char*)"TRUE");
    
    //Node attributes
    agattr(main_graph, AGNODE, (char*)"shape", (char*)"none");
    agattr(main_graph, AGNODE, (char*)"style", (char*)"filled");
    agattr(main_graph, AGNODE, (char*)"fillcolor", (char*)"white");
    agattr( main_graph, AGNODE, (char*)"fontsize", (char*)"16");
    
    
    agattr( main_graph, AGEDGE, (char*)"penwidth", (char*)"1");
    agattr( main_graph, AGEDGE, (char*)"color", (char*)"black");
    agattr( main_graph, AGEDGE, (char*)"fontsize", (char*)"16");
    
    input_subgraph = agsubg(main_graph, (char*)"input", true);
    agattr(input_subgraph, AGRAPH, (char*)"rank", (char*)"same");
    
    gvLayout(gvc, main_graph, "dot");
}

Script::Script(char const *path){
    
    FILE *file;
    
    init();
    
    SYSTEM_ERROR_CHECK(file=fopen(path, "r"), NULL, "Error opening '%s'", path);
    main_graph = agread(file, NULL);
    SYSTEM_ERROR_CHECK(fclose(file), -1, NULL);
    generate_layout();
}


void Script::add_process(Program *program, char **argv){
    gvFreeLayout(gvc, main_graph);
    new Process(this, program, argv);
    generate_layout();
}

void Script::add_link( Process *input_process, char *output_channel, Process *output_process, char *input_channel, int sync){
    gvFreeLayout(gvc, main_graph);
    new Link(this, input_process, output_channel, output_process, input_channel, sync);
    generate_layout();
}

int Script::unselect_all(){
    int unselected_items_nb=0;
    while(selected_processes_nb){
        selected_processes[0]->unselect();
        unselected_items_nb++;
        
    }
    while(selected_links_nb){
        selected_links[0]->unselect();
        unselected_items_nb++;
    }
    return unselected_items_nb;
}

void Script::save_bash(char const *path){
    char *command;
    FILE *file;
    Process **process_pt;
    
    SYSTEM_ERROR_CHECK(file=fopen(path, "w"), NULL, "Opening '%s'", path);
    
    fprintf(file, "source blaar.sh\n\n");
    
    FOR_EACH(process_pt, processes, processes_nb){
        if (process_pt[0]->program->type==INPUT_PROGRAM){
            command=blc_create_command_line_from_argv(process_pt[0]->argv);
            fprintf(file, "%s |\n", command);
            //     if (process_pt[0]->program->
            free(command);
        }
    }
    SYSTEM_ERROR_CHECK(fclose(file), -1, "Closing '%s'", path);
}

void Script::update_display(){
    boxf graph_box;
    
    graph_box=GD_bb(main_graph);
    width=graph_box.UR.x;
    height=graph_box.UR.y;
    
    gtk_layout_set_size(GTK_LAYOUT(drawing_area), width, height);
    gtk_widget_queue_draw(widget);
}

Process *Script::get_process(double x, double y){
    Process **process_pt;
    boxf node_box;
    
    y=height-y;
    
    FOR_EACH(process_pt, processes, processes_nb){
        node_box = ND_bb(process_pt[0]->node);
        if (x > node_box.LL.x && x < node_box.UR.x && y > node_box.LL.y && y < node_box.UR.y) return process_pt[0];
    }
    return NULL;
}

Link *Script::get_link(double x, double y){
    
    Link **link_pt;
    pointf pos;
    
    y=height-y;
    
    FOR_EACH(link_pt, links, links_nb){
        pos=ED_label(link_pt[0]->edge)->pos;
        if ((fabs(x - pos.x) < 10) && (fabs(y - pos.y) < 10)) return link_pt[0];
    }
    return NULL;
}

void process_write(Process *process, GtkTextBuffer *buffer){
    char *command_line;
    int first=1;
    Link **link_pt;
    Process **process_pt;
    
    command_line=blc_create_command_line_from_argv(process->argv);
    gtk_text_buffer_insert_at_cursor(buffer, command_line, -1);
    FREE(command_line);
    FOR_EACH(link_pt, process->output_links, process->output_links_nb){
        gtk_text_buffer_insert_at_cursor(buffer, " | ", -1);
        process_write(link_pt[0]->output_process, buffer);
    }
}

void Script::generate_layout(){
    cairo_surface_t *surface;
    cairo_t *cr;
    int first=1;
    Process **process_pt;
    GtkTextBuffer *buffer;
    
    buffer=gtk_text_view_get_buffer (GTK_TEXT_VIEW(source_view));
    gtk_text_buffer_set_text(buffer, "", -1);
    gvLayout(gvc, main_graph, "dot");


    //Tricks to update node position;
    surface=cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 1000, 1000);
    cr=cairo_create(surface);
    gvRenderContext(gvc, main_graph, "svg:cairo", cr);
    cairo_destroy(cr);
    update_display();

    FOR_EACH(process_pt, processes, processes_nb) {
        if (process_pt[0]->input_links_nb==0){
            if (first==0) gtk_text_buffer_insert_at_cursor(buffer, " |\n", -1);
            else first=0;
            process_write(process_pt[0], buffer);
        }
        process_pt[0]->update_layout();
    }
}

void recursive_process_run(Process *process, int input_pipe){
    Link **link_pt;
    
    process->run(input_pipe);
    FOR_EACH(link_pt, process->output_links, process->output_links_nb){
        recursive_process_run(link_pt[0]->output_process, process->stdout_fileno);
        process->stdout_fileno=-1; //It will be read by the pipe
    }
}

void recursive_process_pause(Process *process){
    Link **link_pt;
    
    process->pause();
    FOR_EACH(link_pt, process->output_links, process->output_links_nb){
        recursive_process_pause(link_pt[0]->output_process);
    }
}

void recursive_process_stop(Process *process){
    Link **link_pt;
    
    process->stop();
    FOR_EACH(link_pt, process->output_links, process->output_links_nb){
        recursive_process_stop(link_pt[0]->output_process);
    }
}

void Script::run(){
    Process **process_pt;
    
    FOR_EACH(process_pt, processes, processes_nb) {
        if (process_pt[0]->input_links_nb==0){
            recursive_process_run(process_pt[0], STDIN_FILENO);
        }
    }
    gtk_widget_set_sensitive(tool_stop, true);
}

void Script::pause(){
    Process **process_pt;
    
    FOR_EACH(process_pt, processes, processes_nb) {
        if (process_pt[0]->input_links_nb==0){
            recursive_process_pause(process_pt[0]);
        }
    }
}

void Script::stop(){
    Process **process_pt;
    
    FOR_EACH(process_pt, processes, processes_nb) {
        if (process_pt[0]->input_links_nb==0){
             recursive_process_stop(process_pt[0]);
        }
    }
    
    gtk_widget_set_sensitive(tool_stop, false);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tool_run_pause), false);
}
