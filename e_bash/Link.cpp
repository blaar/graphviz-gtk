//
//  Link.cpp
//  e_bash
//
//  Created by Arnaud Blanchard on 30/08/2017.
//
//

#include "common.h"
#include "Link.h"
#include "Process.h"
#include <blc_core.h> //APPEND_ITEM

#include <gtk/gtk.h>


Link::Link(Script *script, Process *input_process, char *output_channel, Process *output_process, char *input_channel, int sync):sync(sync),script(script), input_process(input_process), output_process(output_process), selected(0){
    Link *link=this;
    
    printf("Input program: %s nodes:%d:%s\n", input_process->program->name, output_process->program->output_channels_nb, output_channel);
    printf("Out program: %s nodes:%d:%s\n", output_process->program->name, output_process->program->input_channels_nb, input_channel);
    
    asprintf(&name, "link_%d", script->links_nb);
    
    edge=agedge(current_script->main_graph, input_process->node, output_process->node, name, 1);
    agsafeset(edge, (char*)"label", (char*)name, (char*)"");
    if (sync==0) {
        agsafeset(edge, (char*)"style", (char*)"dashed", (char*)"");
        agsafeset(edge, (char*)"constraint", (char*)"false", (char*)"");

    }
    agsafeset(edge,(char*)"tailport",output_channel, (char*)"");
    agsafeset(edge,(char*)"headport",input_channel, (char*)"");
    APPEND_ITEM(&script->links, &script->links_nb, &link);
    APPEND_ITEM(&input_process->output_links, &input_process->output_links_nb, &link);
    APPEND_ITEM(&output_process->input_links, &output_process->input_links_nb, &link);

}

Link::~Link(){
    Link *tmp_link=this;
    
    REMOVE_ITEM(&input_process->output_links, &input_process->output_links_nb, &tmp_link);
    REMOVE_ITEM(&output_process->input_links, &output_process->input_links_nb, &tmp_link);

    if (selected)   REMOVE_ITEM(&script->selected_links,&script->selected_links_nb, &tmp_link);
    REMOVE_ITEM(&script->links,&script->links_nb, &tmp_link);
    agdelete(script->main_graph, edge);
    free(name);
}

Link *Link::dialog(Script *script, Process *input_process, Process *output_process){
    char **channel;
    Link *link;
    
    GtkWidget  *dialog, *box, *in_combobox, *out_combobox, *sync_checkbox;
    GtkWidget  *grid;
    
    dialog=gtk_dialog_new_with_buttons("New link", NULL, GTK_DIALOG_MODAL,  "_OK", GTK_RESPONSE_ACCEPT, "_Cancel", GTK_RESPONSE_REJECT, NULL);
    box=gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    
    grid=gtk_grid_new();
    
    in_combobox=gtk_combo_box_text_new();
    out_combobox=gtk_combo_box_text_new();
    sync_checkbox=gtk_check_button_new();
    
    gtk_grid_attach(GTK_GRID(grid),  gtk_label_new(input_process->program->name), 0, 0, 1, 1);
    FOR_EACH(channel, input_process->program->output_channels, input_process->program->output_channels_nb) gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(in_combobox), NULL, *channel);
    
    gtk_grid_attach(GTK_GRID(grid),  gtk_label_new(output_process->program->name), 0, 1, 1, 1);
    FOR_EACH(channel, output_process->program->input_channels, output_process->program->input_channels_nb) gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(out_combobox), NULL, *channel);
    
    gtk_grid_attach(GTK_GRID(grid),  gtk_label_new("Synchronize"), 0, 2, 1, 1);

    
    gtk_combo_box_set_active(GTK_COMBO_BOX(in_combobox), 0);
    gtk_combo_box_set_active(GTK_COMBO_BOX(out_combobox), 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sync_checkbox), 1);
    
    gtk_grid_attach(GTK_GRID(grid), in_combobox, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), out_combobox, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(grid), sync_checkbox, 1, 2, 1, 1);

    
    gtk_container_add(GTK_CONTAINER(box), grid);
    gtk_widget_show_all(dialog);
    
    switch (gtk_dialog_run(GTK_DIALOG(dialog)))
    {
        case GTK_RESPONSE_ACCEPT:
            link=new Link(script, input_process, gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(in_combobox)),
                          output_process, gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(out_combobox)),
                          gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sync_checkbox)));
            break;
        case GTK_RESPONSE_CANCEL:
            link=NULL;
            break;
    }
    gtk_widget_destroy(dialog);
    
    return link;
}


void Link::edit(){
    
    dialog(script, input_process, output_process);
    
}


void Link::select(){
    Link *tmp_link;
    if (selected==0){
        agsafeset( edge, (char*) "penwidth", (char*) "3", "1");

      //  agsafeset( edge, (char*) "color", (char*) "red", "black");
        tmp_link=this;
        APPEND_ITEM(&script->selected_links, &script->selected_links_nb, &tmp_link);
        selected=1;
    }
}

void Link::unselect(){
    Link *tmp_link;

    if (selected){
    /*    if (sync) agset((void*) edge, (char*) "style", (char*) "dashed");
        else agset((void*) edge, (char*) "style", (char*) "solid");*/
  //      agsafeset((void*) edge, (char*) "color", (char*) "black", "black");
        agsafeset( edge, (char*) "penwidth", (char*) "1", "1");


        tmp_link=this;

        REMOVE_ITEM(&script->selected_links, &script->selected_links_nb, &tmp_link);
        selected=0;
    }
    
}




