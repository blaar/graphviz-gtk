//
//  gv_render.h
//  e_bash
//
//  Created by Arnaud Blanchard on 25/08/2017.
//
//

#ifndef gv_render_h
#define gv_render_h

#include <gvc.h>
#include <cairo.h>

void gv_cairo_render(GVC_t *gvc, Agraph_t *graph, cairo_t *cr);

#endif /* gv_render_h */
